import React, { Fragment, useState } from 'react'
import { Button, ButtonGroup, UncontrolledTooltip, Modal, ModalHeader, ModalBody, ModalFooter, Table, Row, Col} from 'reactstrap'
import { Link, useParams } from 'react-router-dom'
import { Eye, Edit, Monitor } from 'react-feather'
import DetailSkp from '../DetailSkp/DetailSkp'

export const PembuatanItem = ({ new_map_skp_id, nama, tahun, periode_mulai, periode_selesai, nm_tipro, status_tipro, tgl_mulai, update_terakhir, pic, trx_approval_id, id_map_mst_wf_tipro }) => {
    const { id } = useParams()
    // const { data: detailtitikskp, error, isLoading, isError } = useQuery(["detail_titik_skp", { new_map_skp_id }], getDetailTitikSkp)
    const [OpenModal, setOpenModal] = useState(false)
    const [basicModal, setBasicModal] = useState(false)


    return (
        <Fragment>
            <td>
                <div className='d-flex'>
                    <ButtonGroup>
                        <Button color='warning' id='detail' className='btn btn-sm'>
                            <Link to={`/skp-summary/1/${new_map_skp_id}/${trx_approval_id}/${id_map_mst_wf_tipro}`}>
                                <Eye size={15}  color='white'/>
                            </Link>
                        </Button>
                        <UncontrolledTooltip placement='top' target='detail'>
                            Detail SKP
                        </UncontrolledTooltip>
                    
                        <Button color='success' id='inputrealisasi' className='btn btn-sm'>
                            <Link to={`/`}>
                                <Edit size={15} color='white'/>
                            </Link>
                        </Button>
                        <UncontrolledTooltip placement='top' target='inputrealisasi'>
                            Input Realisasi
                        </UncontrolledTooltip>

                        <Button color='info' id='detail' className='btn btn-sm' onClick={() => setBasicModal(!basicModal)}>
                            <Monitor size={15}  color='white'/>
                        </Button>
                        <UncontrolledTooltip placement='top' target='detail'>
                            Detail SKP
                        </UncontrolledTooltip>
                    </ButtonGroup>
                </div>
            </td>
            <td>{nama}</td>
            <td>{tahun}</td>
            <td>{periode_mulai}</td>
            <td>{periode_selesai}</td>
            <td>{nm_tipro}</td>
            <td>{status_tipro}</td>
            <td>{tgl_mulai}</td>
            <td>{update_terakhir}</td>
            {/* {/ <td>{nama}</td> /} */}
            
            <Modal isOpen={basicModal} toggle={() => setBasicModal(!basicModal)} className='modal-xl'>
                <ModalHeader toggle={() => setBasicModal(!basicModal)}>History SKP</ModalHeader>
                <ModalBody>
                    {/* {/ <h5>Check First Paragraph</h5> /} */}
                    <DetailSkp id={`${new_map_skp_id}`} />

                </ModalBody>
                <ModalFooter>
                    <Button color='primary' onClick={() => setBasicModal(!basicModal)}>
                    Accept
                    </Button>
                </ModalFooter>
            </Modal>

        </Fragment>
    )
}