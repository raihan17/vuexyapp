import axios from "axios"
import Swal from "sweetalert2"
import baseApi from "./baseApi"

export const getListSkp = async () => {

    const response = await baseApi.get(`/rekap-progres-skp`).then((res) => res.data.data)
    // console.log(response)
    if (!response) {
        throw new Error(response.message)
    }

    return response
}

export const buatPembuatan = async (data) => {
    const response = await baseApi.post(`/trx-map-skp-pegawai`, {
        api_pegawai_idp: 'herb001',
        tahun: data.tahun,
        periode_mulai: data.periode_mulai,
        periode_selesai: data.periode_selesai,
        idp_atasan_langsung: data.idp_atasan_langsung,
        idp_atasan_atasan_langsung: data.idp_atasan_atasan_langsung,
        // dok_matriks_peran: data.dok_matriks_peran,
        create_by: 'herb001'
    }).then(res => res.data.data)
    .catch(err => {
        console.log(err)
    })

    if (!response) {
        throw new Error(response.message)
    }
    return response
}

export const submitNext = async (data) => {
    const response = await baseApi.post('/procedure-submit-next', {
        grup: data.grup,
        curr_trx_approval_id: data.curr_trx_approval_id,
        id_transaksi: data.id_transaksi,
        curr_mst_map_wf_tipro_id: data.curr_mst_map_wf_tipro_id,
        curr_status_id: data.curr_status_id,
        submitter: data.submitter,
        notes: data.notes,
        tgl_submit: data.tgl_submit
    }).then((res) => res.data.data)
    .catch(err => {
        console.log('error submit next', err)
    })
    if (!response) {
        throw new Error(response.message)
    }
    return response
}

export const inputDataSkp = (data) => {
    let id_ref
    if (data.ref_sumber_id === 'PK') {
        id_ref = 1
    } else if (data.ref_sumber_id === 'Renstra') {
        id_ref = 2
    } else if (data.ref_sumber_id === 'RKT') {
        id_ref = 3
    } else if (data.ref_sumber_id === 'Intervensi Atasan') {
        id_ref = 9
    }
    const response = axios.post(`http://localhost:8000/api/mst-skp`, {
        api_unor_id: 1013,
        api_jabatan_jenjang_id: 166,
        tahun: data.tahun,
        grup: data.grup,
        ref_sumber_id: id_ref,
        sumber_ref: data.sumber_ref,
        rencana_kinerja: data.rencana_kinerja,
        ref_cascade_type_id: data.ref_cascade_type_id,
        status_skp: 0,
        is_kkm: data.is_kkm,
        ref_kkm_id: data.ref_kkm_id,
        is_override: data.is_override,
        ref_lingkup_id: data.ref_lingkup_id,
        create_by: 'herb001'
    })
    if (!response) {
        throw new Error(response.message)
    }
    return response
}

// export const inputDataIki = async () => {
//     const response = await baseApi.post('/trx-skp-detail')
//     .then((res) => res.data.data)
//     .catch(err => {
//         alert(err)
//     })

//     if (!response) {
//         throw new Error(response.message)
//     }
//     return response
// }

export const getDetailTitikSkp = async ({ queryKey }) => {
    
    const [_key, { id }] = queryKey
    // console.log(response)
    const response = await baseApi.get(`/detil-titik-skp/${id}`).then((res) => res.data.data)
    if (!response) {
        throw new Error(response.message)
    }

    return response
}

export const getSkpSummary = async ({ queryKey }) => {
    
    const [_key, { id, idsearch }] = queryKey
    
    const response = await baseApi.get(`/skp-summary/${id}/${idsearch}`).then((res) => res.data.data)
    
    // console.log(response)
    if (!response) {
        throw new Error(response.message)
    }

    return response
}

export const getkRefSumber = async () => {
    const response = await baseApi.get('/ref-sumber').then((res) => res.data.data)
    return response
}

export const getViewDupakSkp = async ({ queryKey }) => {
    const [_key, { idsearch }] = queryKey
    const response = await baseApi.get(`/dupak-skp/${idsearch}`).then((res) => res.data.data)
    return response
}

export const getViewRekapProgresDupak = async ({ queryKey }) => {
    const [_key, { idpegawai }] = queryKey
    const response = await baseApi.get(`/rekap-progres-dupak/${idpegawai}`).then((res) => res.data.data)
    return response
}

export const getViewDetailTitikDupak = async ({ queryKey }) => {
    const [_key, { id_paketan_dupak_skp }] = queryKey
    const response = await baseApi.get(`/detil-titik-dupak/${id_paketan_dupak_skp}`).then((res) => res.data.data)
    return response
}

export const getTrxApprovalDupakByIdMapSkp = async ({ queryKey }) => {
    const [_key, { idsearch }] = queryKey
    const response = await baseApi.get(`/trx-approval-dupak-by-idmapskp/${idsearch}`).then((res) => res.data.data)
    return response
}

export const getTrxMapSkp = async ({ queryKey }) => {
    const [_key, { id }] = queryKey
    const response = await baseApi.get(`/trx-map-skp-pegawai/${id}`).then((res) => res.data.data)
    return response
}

export const resetApproval = async ({ queryKey }) => {
    const [_key, { jenis, id, idp }] = queryKey
    const respons = await baseApi.post(`/procedure-reset-approval/${jenis}/${id}/${idp}`).then((res) => res.data.data)
}

export const getListInboxSkp = async ({ queryKey}) => {
    const [_key, { idp }] = queryKey
    const response = await baseApi.get(`/rekap-progres-skp/${idp}`).then((res) => res.data.data)
    
    if (!response) {
        throw new Error(response.message)
    }

    return response
}

export const getReviewDataInboxSkp = async ({ queryKey}) => {
    const [_key, { id }] = queryKey
    const response = await baseApi.get(`/mst-skp/${id}`).then((res) => res.data.data)
    
    if (!response) {
        throw new Error(response.message)
    }

    return response
}

export const getReviewDataInboxIki = async ({ queryKey}) => {
    const [_key, { id_iki }] = queryKey
    const response = await baseApi.get(`/trx-skp-detail/${id_iki}`).then((res) => res.data.data)
    
    if (!response) {
        throw new Error(response.message)
    }

    return response
}