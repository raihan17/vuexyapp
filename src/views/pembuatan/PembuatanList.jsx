import React, { Fragment } from 'react'
import { useQuery } from 'react-query'
import { getListSkp } from '../../services/apiPembuatan'

import Breadcrumbs from '@components/breadcrumbs'
import { Row, Col, Card, CardTitle, CardHeader, Table, Button} from 'reactstrap'
import Loader from 'react-loader-spinner'
import { Link } from 'react-router-dom'
import { PembuatanItem } from './PembuatanItem'

export const PembuatanList = () => {
    const { data, error, isLoading, isError } = useQuery("pembuatanskp", getListSkp)

    if (isLoading) {
        return (
            <Fragment>
                <Row>
                    <Col md="12">
                        <div className='text-center py-5 justify-content-center'>
                            <Loader type="ThreeDots" color="#222" height={30}/>
                        </div>
                    </Col>
                </Row>
            </Fragment>
        )
    }

    if (isError) {
        return <span>Error: {error}</span>
    }

    return (
        <Fragment>
            <Breadcrumbs breadCrumbTitle='Pembuatan SKP' breadCrumbParent='Home' breadCrumbActive='SKP' />
            <Row>
                <Col md="12">
                    <Card>
                        <CardHeader>
                            <CardTitle>Pembuatan SKP</CardTitle>
                            <div className='d-flex float-right'>
                                <Link to={'/create-skp'}>
                                    <Button color='primary'>Buat SKP</Button>
                                </Link>
                            </div>
                        </CardHeader>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nama Pegawai</th>
                                    <th>Tahun</th>
                                    <th>Periode Mulai</th>
                                    <th>Periode Selesai</th>
                                    <th>Titik Proses Akhir</th>
                                    <th>Status</th>
                                    <th>Start</th>
                                    <th>Last Update</th>
                                    {/* {/ <th>PJ</th> /} */}
                                </tr>
                            </thead>
                            <tbody>
                            {
                                data.map(({trx_map_skp_pegawai_id, nama, tahun, periode_mulai, periode_selesai, nm_tipro, status_tipro, tgl_mulai, update_terakhir, pic, trx_approval_id, id_map_mst_wf_tipro}) => (
                                <tr key={trx_map_skp_pegawai_id}>
                                    <PembuatanItem 
                                        new_map_skp_id={trx_map_skp_pegawai_id} 
                                        nama={nama} 
                                        tahun={tahun} 
                                        periode_mulai={periode_mulai} 
                                        periode_selesai={periode_selesai} 
                                        nm_tipro={nm_tipro}
                                        status_tipro={status_tipro} 
                                        tgl_mulai={tgl_mulai}
                                        update_terakhir={update_terakhir}
                                        pic={pic}
                                        trx_approval_id={trx_approval_id}
                                        id_map_mst_wf_tipro={id_map_mst_wf_tipro}
                                    />
                                </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                    </Card>
                </Col>
            </Row>
        </Fragment>
    )
}