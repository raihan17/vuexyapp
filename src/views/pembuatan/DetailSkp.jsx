import { Fragment, useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { Col, Row, Table } from 'reactstrap'
import { getDetailTitikSkp } from '../../services/apiPembuatan'
import Loader from 'react-loader-spinner'

const DetailSkp = (props) => {

    const [idMapSkp, setIdMapSkp] = useState()

    useEffect(() => {
        setIdMapSkp(props)
    }, [])

    const { data, error, isLoading, isError } = useQuery(["detail_titik_skp", idMapSkp], getDetailTitikSkp)
    
    if (isLoading) {
        return (
            <Fragment>
                <Row>
                    <Col md="12">
                        <div className='text-center py-5 justify-content-center'>
                            <Loader type="ThreeDots" color="#222" height={30}/>
                            <span className='text-success'>Sedang mengambil data..</span>
                        </div>
                    </Col>
                </Row>
            </Fragment>
        )
    }

    if (isError) {
        return <span>Error: {error.message}</span>
    }

    return (
        
        <Table responsive>
            <thead>
                <tr>
                    <th>Titik Proses</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th>Start</th>
                    <th>Last Update</th>
                    <th>PIC</th>
                    <th>Catatan</th>
                </tr>
            </thead>
            <tbody>
                {
                    data?.map(({ index, tipro, pj, status, catatan, tgl_start, tgl_last_update, pengubah_trakhir }) => (
                        <tr key={index}>
                            <td>{tipro}</td>
                            <td>{pj}</td>
                            <td>{status}</td>
                            <td>{tgl_start}</td>
                            <td>{tgl_last_update}</td>
                            <td>{pengubah_trakhir}</td>
                            <td>{catatan}</td>
                        </tr>
                    ))
                }
            </tbody>
        </Table>
            
    )
}

export default DetailSkp